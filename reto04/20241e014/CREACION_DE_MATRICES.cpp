#include <iostream>
#include <conio.h>

using namespace std;
using namespace System;

int* CreacionMatriz(int n, int m) {
    int* Mtrz = new int[n * m];
    for (int i = 0; i < n * m; ++i) {
        Mtrz[i] = 0;
    }

    return Mtrz;
}

void ImprimirMatriz(int* Mtrz, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << Mtrz[i * m + j] << " ";
        }
        cout << endl;
    }
}

void OutMatriz(int* Mtrz) {
    delete[] Mtrz;
}

int main() {
    int n, m;

    cout << "Ingrese el numero de las filas (n): ";
    cin >> n;
    cout << "Ingrese el numero de las columnas (m): ";
    cin >> m;

    int* Mtrz = CreacionMatriz(n, m);

    cout << "Matriz de los intervalos " << n << " x " << m << " inicializada con ceros:" << endl;
    ImprimirMatriz(Mtrz, n, m);

    OutMatriz(Mtrz);
    _getch();
    return 0;
}